//controller
const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Product = require("../models/Product");
const Order = require("../models/Order");


//User Registration 
/*
	Steps:
	1. create a new user object
	2. make sure that the password is encrypted
	3. save the new User to the database
*/

module.exports.registerUser = (reqbody) =>{

	let newUser = new User({
		email:reqbody.email,
		password: bcrypt.hashSync(reqbody.password,10),
	})

	return newUser.save().then((user,error)=>{

		if(error){
			return false;
		}else{
			return true
		}

	})
	.catch(err=>err)
}

//User authentication 
/*
Steps: 
1. Check the db if user email exists
2. Compare the password from req.body and email from db
3. Generate a JWT if the user logged in and return false if not
*/

module.exports.loginUser = (req,res)=>{
	return User.findOne({email:req.body.email}).then(result=>{
		if(result === null){
			return false;
		}else{
			//compareSync method is used to compare a non-encrypted password and from the encrypted password from the db
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password)
			if(isPasswordCorrect){
				return res.send({access: auth.createAccessToken(result)})
			}
			//else password does not match
			else{
				return res.send(false);
			}
		}
	})
	.catch(err=>res.send(err))
}


// s52 Create order controller 
module.exports.checkoutProduct = async (req, res) => {
  try {
    const { userId, productName, quantity } = req.body;

    // Check if the user is an admin
    if (req.user.isAdmin) {
      return res.send("Admin users are forbidden to checkout.");
    }

    // Find the product by name (case-insensitive and partial match)
    const productMatch = await Product.aggregate([
      {
        $match: {
          name: { $regex: new RegExp(productName, 'i') }
        }
      }
    ]);

    if (productMatch.length === 0) {
      return res.send({ message: `Product '${productName}' not found.` });
    }

    const product = productMatch[0];

    const totalAmount = product.price * quantity;

    // Create the order document
    const order = await Order.create({
      userId: userId,
      products: [
        {
          productId: product._id,
          productName: product.name, 
          quantity: quantity,
        }
      ],
      totalAmount: totalAmount,
      purchasedOn: new Date(),
    });

    // Update the user's orders array
    await User.findByIdAndUpdate(userId, { $addToSet: { orders: order._id } }, { upsert: true });
    console.log('Order created successfully:', order);

    res.send({ message: "Order created successfully.", order: order });
  } catch (error) {
    console.error('Error creating the order:', error);
    res.send({ message: "An error occurred while creating the order." });
  }
};

//s52 Retrieve user details controller 
module.exports.getProfile=(req,res) =>{

    return User.findById(req.user.id)
    .then(result => {
      result.password = "";
      return res.send(result);
    })
    .catch(err=>res.send(err))
  };

//Stretch goals - Set user as admin (admin only)
module.exports.updateUserAsAdmin = async (req, res) => {
  const userIdToUpdate = req.params.userId;

  try {
    const updatedUser = await User.findByIdAndUpdate(
      userIdToUpdate,
      { isAdmin: true }, // Set isAdmin to true to update user as admin
      { new: true }
    );

    if (!updatedUser) {
      return res.status(404).json({ message: "User not found" });
    }

    res.json({ message: "User updated as admin successfully" });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Failed to update user as admin" });
  }
};


//Stretch Goals - Retrieve authenticated user’s orders controller
module.exports.getOrders = async (req, res) => {
  try {
    // Assuming the user information is available in req.user from the JWT token
    const loggedInUserId = req.user.id;

    // Find all orders made by the user
    const orders = await Order.find({ userId: loggedInUserId }).populate('products.productId');

    // Map orders to include product names
    const ordersWithProductNames = await Promise.all(orders.map(async order => {
      const productsWithNames = await Promise.all(order.products.map(async product => {
        const productDetails = await Product.findById(product.productId);
        return {
          productId: productDetails._id,
          productName: productDetails.name,
          productPrice: productDetails.price,
          quantity: product.quantity
        };
      }));

      return {
        _id: order._id,
        userId: order.userId,
        products: productsWithNames,
        totalAmount: order.totalAmount,
        purchasedOn: order.purchasedOn
      };
    }));

    res.status(200).json({ orders: ordersWithProductNames });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'An error occurred while retrieving orders.' });
  }
};

//Reset Password

  module.exports.resetPassword = async (req, res) => {
    try {

      const { newPassword } = req.body;
      const { id } = req.user; // Extracting user ID from the authorization header
    
      // Hashing the new password
      const hashedPassword = await bcrypt.hash(newPassword, 10);
    
      // Updating the user's password in the database
      await User.findByIdAndUpdate(id, { password: hashedPassword });
    
      // Sending a success response
      res.status(200).send({ message: 'Password reset successfully' });
    } catch (error) {
      console.error(error);
      res.status(500).send({ message: 'Internal server error' });
    }
  };








