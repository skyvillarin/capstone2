const Product = require("../models/Product");
const User = require("../models/User");

// Create Product Controller 
module.exports.addProduct = (req, res) => {
  let newProduct = new Product({
    name: req.body.name,
    description: req.body.description,
    price: req.body.price,
  });

  return newProduct.save()
    .then((product) => {
      return res.status(200).json({ success: true, message: "New product created!" });
    })
    .catch((err) => {
      return res.status(500).json({ success: false, message: err.message });
    });
};


// Retrieve all products controller 
module.exports.getAllProducts = (req,res)=>{
  return Product.find({}).then(result=>{
    return res.send(result)
  })
};

// Retrieve all active products controller 
module.exports.getAllActiveProducts = (req,res)=>{
  return Product.find({isActive:true}).then(result=>{
    return res.send(result)
  })
};

// Retrieve single product controller 
module.exports.getProduct = (req,res)=>{
  return Product.findById(req.params.productId).then(result=>{
    return res.send(result)
  })
};

// Update product info admin only controller 
module.exports.updateProduct = (req,res)=>{

  let updatedProduct = {
    name: req.body.name,
    description: req.body.description,
    price: req.body.price
  };

  return Product.findByIdAndUpdate(req.params.productId, updatedProduct).then((product, error)=>{
    if(error){
      return res.send("Error updating product.");
    }else{
      return res.send("Product successfully updated.");
    }
  })
  .catch(err => res.send(err))
};


// Archive product admin only controller 
module.exports.archiveProduct = (req, res) => {
  let updatedIsActive = {
    isActive: false
  };

  Product.findByIdAndUpdate(req.params.productId, updatedIsActive)
    .then((product, error) => {
      if (error) {
        return res.status(500).json({ success: false, message: "Error archiving product." });
      } else {
        return res.status(200).json({ success: true, message: "Product successfully archived." });
      }
    })
    .catch((err) => res.status(500).json({ success: false, message: err.message }));
};



// Activate product admin only controller 
  module.exports.activateProduct = (req, res) => {
  let updatedIsActive = {
    isActive: true
  };

  Product.findByIdAndUpdate(req.params.productId, updatedIsActive)
    .then((product, error) => {
      if (error) {
        return res.status(500).json({ success: false, message: "Error activating product." });
      } else {
        return res.status(200).json({ success: true, message: "Product successfully activated." });
      }
    })
    .catch((err) => res.status(500).json({ success: false, message: err.message }));
};

/*module.exports.activateProduct = (req, res) => {
  return Product.findByIdAndUpdate(req.params.productId)
    .then(savedProduct => {
      if (!savedProduct) {
        return res.send("Product not found."); 
      }
      if (savedProduct.isActive) {
        return res.send("Product is already active."); 
      }
      savedProduct.isActive = true;
      savedProduct.save()
        .then(() => {
          return res.send("Product activated successfully."); 
        })
        .catch(saveError => {
          console.error(saveError);
          return res.send("Error activating product."); 
        });
    })
    .catch(error => {
      console.error(error);
      return res.send("Error executing command."); 
    });
};*/

// Search products by name
module.exports.searchProductsByName = async (req, res) => {
  try {
    const { productName } = req.body;
  
    // Use a regular expression to perform a case-insensitive search
    const products = await Product.find({
    name: { $regex: productName, $options: 'i' }
    });
  
    res.json(products);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
};

// Search products by price range

module.exports.searchProductsByPriceRange = async (req, res) => {
    try {
      const { minPrice, maxPrice } = req.body;
    
      // Find products within the price range
      const products = await Product.find({
      price: { $gte: minPrice, $lte: maxPrice }
      });
    
      res.status(200).json({ products });
    } catch (error) {
      res.status(500).json({ error: 'An error occurred while searching' });
    }
    };





