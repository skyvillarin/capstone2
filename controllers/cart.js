//controller
const User = require("../models/User");
const Product = require("../models/Product");
const Cart = require('../models/Cart');
const bcrypt = require("bcrypt");
const auth = require("../auth");


//Stretch Goals - Add to cart - Added products controller
module.exports.addToCart = async (req, res) => {
  try {
    // Assuming the user information is available in req.user from the JWT token
    const loggedInUserId = req.user.id;

    const { products } = req.body;

    const user = await User.findById(loggedInUserId);
    if (!user) {
      return res.status(404).json({ message: 'User not found.' });
    }

    const cart = await Cart.findOne({ userId: user._id }) || new Cart({ userId: user._id, products: [], total: 0 });

    for (const product of products) {
      const productMatch = await Product.aggregate([
        {
          $match: {
            name: { $regex: new RegExp(product.productName, 'i') }
          }
        }
      ]);

      if (productMatch.length === 0) {
        return res.status(404).json({ message: `Product '${product.productName}' not found.` });
      }

      const existingCartItem = cart.products.find(cartItem =>
        cartItem.productId.toString() === productMatch[0]._id.toString()
      );

      if (existingCartItem) {
        existingCartItem.quantity += product.quantity;
        existingCartItem.subtotal += product.quantity * productMatch[0].price;
      } else {
        cart.products.push({
          productId: productMatch[0]._id,
          productName: productMatch[0].name,
          quantity: product.quantity,
          subtotal: product.quantity * productMatch[0].price
        });
      }

      cart.total += product.quantity * productMatch[0].price;
    }

    await cart.save();

    res.status(200).json({ message: 'Products added to cart successfully.', cart: cart });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'An error occurred while adding to cart.' });
  }
};

// Update quantity controller 
module.exports.updateQuantity = async (req, res) => {
  try {
    // Assuming the user information is available in req.user from the JWT token
    const loggedInUserId = req.user.id;

    const { productName, quantity } = req.body;

    const user = await User.findById(loggedInUserId);

    if (!user) {
      return res.status(404).json({ message: 'User not found.' });
    }

    const cart = await Cart.findOne({ userId: user._id });

    if (!cart) {
      return res.status(404).json({ message: 'Cart not found.' });
    }

    const productMatch = await Product.aggregate([
      {
        $match: {
          name: { $regex: new RegExp(productName, 'i') }
        }
      }
    ]);

    if (productMatch.length === 0) {
      return res.status(404).json({ message: `Product '${productName}' not found.` });
    }

    const product = productMatch[0]; // Use the first matched product

    const cartItem = cart.products.find(item =>
      item.productId.toString() === product._id.toString()
    );

    if (!cartItem) {
      return res.status(404).json({ message: `Product '${productName}' not found in cart.` });
    }

    cartItem.quantity = quantity;
    cartItem.subtotal = product.price * quantity;

    const total = cart.products.reduce((acc, item) => acc + item.subtotal, 0);
    cart.total = total;

    await cart.save();

    res.status(200).json({
      message: 'Product quantity updated successfully.',
      cart: {
        _id: cart._id,
        userId: cart.userId,
        products: cart.products,
        total: cart.total
      }
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'An error occurred while updating the product quantity.' });
  }
};


// Remove product controller

module.exports.removeProduct = async (req, res) => {
  try {
    // Assuming the user information is available in req.user from the JWT token
    const loggedInUserId = req.user.id;

    const { productName } = req.body;

    const user = await User.findById(loggedInUserId);

    if (!user) {
      return res.status(404).json({ message: 'User not found.' });
    }

    const cart = await Cart.findOne({ userId: user._id });

    if (!cart) {
      return res.status(404).json({ message: 'Cart not found.' });
    }

    const productMatch = await Product.aggregate([
      {
        $match: {
          name: { $regex: new RegExp(productName, 'i') }
        }
      }
    ]);

    if (productMatch.length === 0) {
      return res.status(404).json({ message: `Product '${productName}' not found.` });
    }

    const product = productMatch[0]; // Use the first matched product

    const cartItemIndex = cart.products.findIndex(item =>
      item.productId.toString() === product._id.toString()
    );

    if (cartItemIndex === -1) {
      return res.status(404).json({ message: `Product '${productName}' not found in cart.` });
    }

    cart.products.splice(cartItemIndex, 1);

    const total = cart.products.reduce((acc, item) => acc + item.subtotal, 0);
    cart.total = total;

    await cart.save();

    res.status(200).json({
      message: 'Product removed from cart successfully.',
      cart: {
        _id: cart._id,
        userId: cart.userId,
        products: cart.products,
        total: cart.total
      }
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'An error occurred while removing the product from the cart.' });
  }
};

// Get subtotal controller
module.exports.getSubtotal = async (req, res) => {
  try {
    // Assuming the user information is available in req.user from the JWT token
    const loggedInUserId = req.user.id;

    const { productName } = req.body;

    const user = await User.findById(loggedInUserId);
    if (!user) {
      return res.status(404).json({ message: 'User not found.' });
    }

    const cart = await Cart.findOne({ userId: user._id });
    if (!cart) {
      return res.status(404).json({ message: 'Cart not found.' });
    }

    const productMatch = await Product.aggregate([
      {
        $match: {
          name: { $regex: new RegExp(productName, 'i') }
        }
      }
    ]);

    if (productMatch.length === 0) {
      return res.status(404).json({ message: `Product '${productName}' not found.` });
    }

    const productsInCart = cart.products.filter(cartItem => {
      const matchingProduct = productMatch.find(product =>
        product._id.toString() === cartItem.productId.toString()
      );
      return matchingProduct;
    });

    let subtotal = 0;
    for (const cartItem of productsInCart) {
      const matchingProduct = productMatch.find(product =>
        product._id.toString() === cartItem.productId.toString()
      );
      subtotal += cartItem.quantity * matchingProduct.price;
    }

    res.status(200).json({ productName, subtotal });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'An error occurred while calculating subtotal.' });
  }
};


// Get Total controller
module.exports.getTotal = async (req, res) => {
  try {
    // Assuming the user information is available in req.user from the JWT token
    const loggedInUserId = req.user.id;

    const user = await User.findById(loggedInUserId);
    if (!user) {
      return res.status(404).json({ message: 'User not found.' });
    }

    const cart = await Cart.findOne({ userId: user._id });
    if (!cart) {
      return res.status(404).json({ message: 'Cart not found.' });
    }

    let total = 0;

    for (const cartItem of cart.products) {
      const matchingProduct = await Product.findById(cartItem.productId);
      if (matchingProduct) {
        total += cartItem.quantity * matchingProduct.price;
      }
    }

    res.status(200).json({ total });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'An error occurred while calculating total.' });
  }
};

// Get items in Cart
module.exports.getItemsInCart = async (req, res) => {
  try {
    const userId = req.user.id; // Use the authenticated user's ID

    // Find the user's cart
    const cart = await Cart.findOne({ userId: userId }).populate('products.productId');

    if (!cart) {
      return res.status(404).json({ message: 'Cart not found.' });
    }

    res.status(200).json({ cart: cart });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'An error occurred while retrieving the cart.' });
  }
};



