const Order = require("../models/Order");
const User = require("../models/User");



// Retrieve all orders controller
module.exports.getAllOrders = (req,res)=>{
  return Order.find({}).then(result=>{
    return res.send(result)
  })
}

