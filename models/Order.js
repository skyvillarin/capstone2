// Dependencies 
const mongoose = require("mongoose");

//Schema
const orderSchema = new mongoose.Schema({
	userId: {
	    type: mongoose.Schema.Types.ObjectId,
	    ref: "User",
	    required: [true, "userId is required"],
  	},
  	products: [
    {
      productId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Product",
        required: [true, "Product ID is required"],
      },
      productName: {
        type: String,
        required: [true, "Product Name is required"],
      }, 
      quantity: {
        type: Number,
        required: [true, "Quantity is required"]
      }
    }
  ],
  totalAmount: {
    type: Number,
    required: [true, "Total Amount is required"]
  },
  purchasedOn: {
    type: Date,
    default: new Date(),
  }
});

//Model
module.exports = mongoose.model("Order", orderSchema);