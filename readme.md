## E-COMMERCE API DOCUMENTATION

***TEST ACCOUNTS:***
- Regular User:
     - email: pennywise@mail.com
     - pwd: pennywise123
- Regular User:
    - email: leatherface@mail.com
    - pwd: leatherface123
- Admin User:
    - email: jigsaw@mail.com
    - pwd: jigsaw123
- Admin User:
    - email: nosferatu@mail.com
    - pwd: nosferatu123
    
    

***ROUTES:***
- User registration (POST)
	- http://localhost:4000/users/register
    - request body: 
        - email (string)
        - password (string)

- User authentication (POST)
	- http://localhost:4000/users/login
    - request body: 
        - email (string)
        - password (string)

- Create Product (Admin only) (POST)
	- http://localhost:4000/products/create
    - request body: 
        - name (string)
        - description (string)
        - price (number)
    - authorization: bearer token of admin

- Retrieve all products (Admin only) (GET)
	- http://localhost:4000/products/all
    - request body: none
    - authorization: bearer token of admin

- Retrieve all active products (GET)
	- http://localhost:4000/products/active
    - request body: none

- Retrieve single product (GET)
	- http://localhost:4000/products/:productId
    - request body: none

- Update Product information (Admin only) (PUT)
	- http://localhost:4000/products/:productId
    - request body: none
    - authorization: bearer token of admin

- Archive Product (Admin only) (PUT)
	- http://localhost:4000/products/:productId/archive
    - request body: none
    - authorization: bearer token of admin

- Activate Product (Admin only) (PUT)
	- http://localhost:4000/products/:productId/activate
    - request body: none
    - authorization: bearer token of admin

- Non-admin User checkout (Create Order)(POST)
	- http://localhost:4000/users/checkout
    - request body: 
    	- userId (string)
        - productName (string)
        - quantity (number)

- Retrieve user details (POST)
	- http://localhost:4000/users/details
    - request body: none
  
- Set user as admin (Admin only) (PUT)
	- http://localhost:4000/users/update-admin/:userId
    - request body: none
    - authorization: bearer token of admin

- Retrieve authenticated user’s orders (GET)
	- http://localhost:4000/users/getOrders
    - request body: none

- Retrieve all orders (Admin only) (GET)
	- http://localhost:4000/orders/all
    - request body: none
    - authorization: bearer token of admin

- Add to cart

	- Added products (POST)
		- http://localhost:4000/carts/add-to-cart
	    - request body:
	        - productName (string)
	        - quantity (number)
        - authorization: bearer token of regular user

    - Change product quantities (PUT)
		- http://localhost:4000/carts/update-quantity
	    - request body:
	        - productName (string)
	        - quantity (number)
        - authorization: bearer token of regular user

    - Remove product (DELETE)
		- http://localhost:4000/carts/remove-product
	    - request body:
	        - productName (string)
        - authorization: bearer token of regular user

    - Subtotal for each item (GET)
		- http://localhost:4000/carts/subtotal
	    - request body:
	        - productName (string)
        - authorization: bearer token of regular user

    - Total for all item (GET)
		- http://localhost:4000/carts/total
	    - request body: none
        - authorization: bearer token of regular user

    - Retrieve cart (GET)
        - http://localhost:4000/carts/
        - request body: none
        - authorization: bearer token of regular user
	































