//Route

//Dependencies and Modules

const express = require("express");
const userController = require("../controllers/user");
const auth = require("../auth");
const {verify, verifyAdmin} = auth; //destructure process

//Routing Component
const router = express.Router();

//Register a user 
router.post("/register",(req,res)=>{
	userController.registerUser(req.body).then(resultFromController=>res.send(resultFromController))
})

//User Authentication 
router.post("/login",userController.loginUser);


// s52 Create Order 
router.post("/checkout", verify, userController.checkoutProduct);

// s52 Retrieve user details 
router.get("/details", verify, userController.getProfile);

//Stretch Goals - Set user as admin (admin only)
router.put("/update-admin/:userId", verify, verifyAdmin, userController.updateUserAsAdmin
);

//Stretch Goals - Retrieve authenticated user’s orders
router.get("/getOrders", verify, userController.getOrders);

//[SECTION] Reset Password
	router.put('/reset-password', verify, userController.resetPassword);

//Export Route System
module.exports = router;