//Dependencies and Modules

const express = require("express");
const productController = require("../controllers/product");
const auth = require("../auth");
const {verify, verifyAdmin} = auth; //destructure process

//Routing Component
const router = express.Router();

// Create Product Admin Only 
router.post("/addProduct", verify, verifyAdmin, productController.addProduct);

// Retrieve all products 
router.get("/all", productController.getAllProducts);

// Retrieve all active products 
router.get("/", productController.getAllActiveProducts);

// Search product by name
router.post('/searchByName', productController.searchProductsByName);

//Search products By Price Range
router.post('/searchByPrice', productController.searchProductsByPriceRange);

// Retrieve specific product 
router.get("/:productId", productController.getProduct);

// Update Product Info Admin only 
router.put("/:productId",verify, verifyAdmin, productController.updateProduct);

// Archive product admin only 
router.put("/:productId/archive", verify, verifyAdmin, productController.archiveProduct);

// Activate product admin only 
router.put("/:productId/activate",verify, verifyAdmin, productController.activateProduct);




//Export Route System
module.exports = router;