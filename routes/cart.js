//Dependencies and Modules

const express = require("express");
const cartController = require("../controllers/cart");
const auth = require("../auth");
const {verify, verifyAdmin} = auth; //destructure process

//Routing Component
const router = express.Router();

//----- Stretch Goals 

// Add products to user's cart (requires authentication)
router.post('/add-to-cart', verify, cartController.addToCart);

// Update product quantity in user's cart (requires authentication)
router.put('/update-quantity', verify, cartController.updateQuantity);

// Remove product
router.delete('/remove-product', verify, cartController.removeProduct);

//Get subtotal
router.get('/subtotal', verify, cartController.getSubtotal);


//Get Total
router.get('/total', verify, cartController.getTotal);

//Get Items in Cart
router.get('/', verify, cartController.getItemsInCart);

//Export Route System
module.exports = router;