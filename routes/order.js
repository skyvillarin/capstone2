//Route

//Dependencies and Modules

const express = require("express");
const orderController = require("../controllers/order");
const auth = require("../auth");
const {verify, verifyAdmin} = auth; //destructure process

//Routing Component
const router = express.Router();

// Stretch goals Retrieve all orders admin only
router.get("/all", verify, verifyAdmin, orderController.getAllOrders);


//Export Route System
module.exports = router;